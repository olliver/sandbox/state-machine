#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0+

FROM registry.hub.docker.com/library/alpine:latest

LABEL Maintainer="oliver@schinagl.nl"

RUN \
	apk add --no-cache \
		gcc \
		make \
		musl-dev \
	&& \
	rm -f /var/cache/apk/*

COPY ./test/ /test/
