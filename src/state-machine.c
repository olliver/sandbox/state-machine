/*
 * (C) Copyright 2018
 * Olliver Schinagl <oliver@schinagl.nl>
 *
 * SPX-License-Identifier:	AGPL-3.0+
 *
 */

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Function pointer helpers to allow for a function that can return a pointer
 * to a function of the same type. See comp.lang.c FAQ list Question 1.22 for
 * more details.
 */
typedef void (*state_ptr)(const uint8_t X, uint8_t *y);
typedef state_ptr (*state_funcptr_t)(const uint8_t X, uint8_t *y);

/* state-machine exit call prototypes */
state_ptr exit_state_A(const uint8_t X, uint8_t *y);
state_ptr exit_state_B(const uint8_t X, uint8_t *y);
state_ptr exit_state_C(const uint8_t X, uint8_t *y);
state_ptr exit_state_D(const uint8_t X, uint8_t *y);

/**
 * exec_state_A - internal function which performs actions related to state A
 *
 * @y:	pointer which will hold the modified output created by state A
 *
 * This function performs all the work that is part of state A.
 *
 * Return:	a function pointer to the current state exit routine
 */
state_ptr exec_state_A(uint8_t *y)
{
	*y = 2;
	return (state_ptr)exit_state_A;
}

/**
 * exec_state_B - internal function which performs actions related to state B
 *
 * @y:	pointer which will hold the modified output created by state B
 *
 * This function performs all the work that is part of state B.
 *
 * Return:	a function pointer to the current state exit routine
 */
state_ptr exec_state_B(uint8_t *y)
{
	*y = 1;
	return (state_ptr)exit_state_B;
}

/**
 * exec_state_C - internal function which performs actions related to state C
 *
 * @y:	pointer which will hold the modified output created by state C
 *
 * This function performs all the work that is part of state C.
 *
 * Return:	a function pointer to the current state exit routine
 */
state_ptr exec_state_C(uint8_t *y)
{
	*y = 3;
	return (state_ptr)exit_state_C;
}

/**
 * exec_state_D - internal function which performs actions related to state D
 *
 * @y:	pointer which will hold the modified output created by state D
 *
 * This function performs all the work that is part of state D.
 *
 * Return:	a function pointer to the current state exit routine
 */
state_ptr exec_state_D(uint8_t *y)
{
	*y = 4;
	return (state_ptr)exit_state_D;
}

/**
 * exit_state_A - function that performs the state transition out of state A
 *
 * @X:	determines which state to transition to
 * @y:	pointer which will hold the modified output created by the next state
 *
 * This function decides which state to transition to, and as a consequence
 * will update the value pointed to by @y.
 *
 * Return:	a function pointer to the new state, NULL on error
 */
state_ptr exit_state_A(const uint8_t X, uint8_t *y)
{
	if (X == 0)
		return exec_state_B(y);

	if (X == 1)
		return exec_state_A(y);

	return NULL;
}

/**
 * exit_state_B - function that performs the state transition out of state B
 *
 * @X:	determines which state to transition to
 * @y:	pointer which will hold the modified output created by the next state
 *
 * This function decides which state to transition to, and as a consequence
 * will update the value pointed to by @y.
 *
 * Return:	a function pointer to the new state, NULL on error
 */
state_ptr exit_state_B(const uint8_t X, uint8_t *y)
{
	if (X == 0)
		return exec_state_B(y);

	if (X == 1)
		return exec_state_C(y);

	return NULL;
}

/**
 * exit_state_C - function that performs the state transition out of state C
 *
 * @X:	determines which state to transition to
 * @y:	pointer which will hold the modified output created by the next state
 *
 * This function decides which state to transition to, and as a consequence
 * will update the value pointed to by @y.
 *
 * Return:	a function pointer to the new state, NULL on error
 */
state_ptr exit_state_C(const uint8_t X, uint8_t *y)
{
	if (X == 0)
		return exec_state_B(y);

	if (X == 1)
		return exec_state_D(y);

	return NULL;
}

/**
 * exit_state_D - function that performs the state transition out of state D
 *
 * @X:	determines which state to transition to
 * @y:	pointer which will hold the modified output created by the next state
 *
 * This function decides which state to transition to, and as a consequence
 * will update the value pointed to by @y.
 *
 * Return:	a function pointer to the new state, NULL on error
 */
state_ptr exit_state_D(const uint8_t X, uint8_t *y)
{
	if (X == 0)
		return exec_state_B(y);

	if (X == 1) {
		return exec_state_A(y);
	}

	return NULL;
}

/**
 * state_init - function that initializes the state machine
 *
 * @y:	pointer which will hold the modified output created by the initial state
 *
 * This function executes the initial state.
 *
 * Return:	a function pointer to the initial state
 */
state_ptr state_init(uint8_t *y)
{
	return exec_state_B(y);
}

int main(void)
{
	int ret = 0;
	state_funcptr_t cur_state = NULL;
	uint8_t X = 0;
	uint8_t y = 0;

	cur_state = (state_funcptr_t)state_init(&y);

	while (true) {
		char c;

		puts("Enter a value [0-1] for X followed by enter. (q to quit)");

		c = getchar();
		getchar(); /* consume enter */

		if (c == 'q')
			break;

		if ((c < '0') || (c > '1')) {
			fputs("Invalid input.", stderr);
			continue;
		}

		X = c - '0';

		cur_state = (state_funcptr_t)cur_state(X, &y);
		if (!cur_state) {
			fputs("Corrupted state machine, cannot continue", stderr);
			ret = -EINVAL;
		}

		printf("X=%u; y=%u\n", X, y);
	}

	if (!ret)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}
